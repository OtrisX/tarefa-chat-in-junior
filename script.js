function postMessage() {
    // Pegar mensagem 
    const message = document.querySelector("#chat_input").value
    
    // Verificar se a mensagem é vazia ou apenas contem espaços, caso verdadeiro retornar
    if(/^\s*$/.test(message)) return

    // Limpar textarea
    document.querySelector("#chat_input").value = ""

    // Fazer POST
    const messageID = Date.now()

    document.querySelector(".chat_log").innerHTML += `
        <div class="message" id="${messageID}">
            <div class="inner_message">
                <p>${message}</p>
            </div>
            <div class="buttons">
                <input class="button_editar" type="button" value="Editar" onclick="editMessage(${messageID})">
                <input class="button_apagar" type="button" value="Apagar" onclick="deleteMessage(${messageID})">
            </div>
        </div>`
}

function deleteMessage(messageID) {
    document.getElementById(messageID).remove()
}

function confirmButton(messageID) {
    const message = document.getElementById(messageID).getElementsByTagName("textarea")[0].value

    // Transformar TextArea em P
    document.getElementById(messageID).getElementsByClassName("inner_message")[0].innerHTML = `<p>${message}</p>`

    // Adicionar botão editar e apagar

    document.getElementById(messageID).getElementsByClassName("buttons")[0].innerHTML = `
        <input class="button_editar" type="button" value="Editar" onclick="editMessage(${messageID})">
        <input class="button_apagar" type="button" value="Apagar" onclick="deleteMessage(${messageID})">
    `
    
}

function editMessage(messageID) {
    // Conteudo da mensagem original
    const oldMessage = document.getElementById(messageID).getElementsByTagName("p")[0].innerHTML 

    // Transformar P em TextArea
    document.getElementById(messageID).getElementsByClassName("inner_message")[0].innerHTML = `<textarea maxlength="280" name="" id="" cols="30" rows="5" style="width: 100%;">${oldMessage}</textarea>`
    
    // Adicionar botão para confirmar edição
    document.getElementById(messageID).getElementsByClassName("buttons")[0].innerHTML = `<input class="button_confirmar" type="button" value="Confirmar" onclick="confirmButton(${messageID})">`
    
}